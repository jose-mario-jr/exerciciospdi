#!/usr/bin/env python3  
# -*- coding: utf-8 -*-

import cv2
import numpy as np

imagemJogador = cv2.imread("soccer.jpg")
# print(imagemJogador)
print(imagemJogador.shape)
print(imagemJogador.item(0, 0, 2), imagemJogador.item(0, 0, 1), imagemJogador.item(0, 0, 0))

imagemJogador.itemset((0, 0, 2), 255)
imagemJogador.itemset((0, 0, 1), 0)
imagemJogador.itemset((0, 0, 0), 0)

bola = imagemJogador[200:270, 270:335]
cv2.imwrite("bola.jpg", bola)
imagemJogador[150:220, 220:285] = bola

cv2.imwrite("soccer3.jpg", imagemJogador)
