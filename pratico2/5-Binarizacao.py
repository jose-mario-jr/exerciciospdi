import numpy as np
import cv2

def binarizacao(img, nome):

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 

    suave = cv2.GaussianBlur(img, (7, 7), 0) # aplica blur
    (T, bin) = cv2.threshold(suave, 100, 255, cv2.THRESH_BINARY)
    
    cv2.imwrite(nome, bin)


img = cv2.imread("manga-1-600.jpg")
binarizacao(img, 'gerado/5/1.png')

img = cv2.imread("manga-2-300x208.jpg")
binarizacao(img, 'gerado/5/2.png')

img = cv2.imread("manga-3-400x280.jpg")
binarizacao(img, 'gerado/5/3.png')

print("\n Sucesso! Threshold: 100 \n Salvo dentro de gerado/5")










