import mahotas
import numpy as np
import cv2

def binarizacaoAdaptativo(img, nome):

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
    suave = cv2.GaussianBlur(img, (7, 7), 0) # aplica blur

    bin1 = cv2.adaptiveThreshold(suave, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 21, 5)
    bin2 = cv2.adaptiveThreshold(suave, 255,
        cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 21, 5)
    resultado = np.vstack([
        np.hstack([img, suave]),
        np.hstack([bin1, bin2])
        ])  

    cv2.imwrite(nome, resultado)

def binarizacaoOtsu(img, nome):

    T = mahotas.thresholding.otsu(suave)
    temp = img.copy()
    temp[temp > T] = 255
    temp[temp < 255] = 0
    temp = cv2.bitwise_not(temp)
    T = mahotas.thresholding.rc(suave)
    temp2 = img.copy()
    temp2[temp2 > T] = 255
    temp2[temp2 < 255] = 0
    temp2 = cv2.bitwise_not(temp2)
    resultado = np.vstack([
        np.hstack([img, suave]),
        np.hstack([temp, temp2])
        ])

    cv2.imwrite(nome, resultado)

img = cv2.imread("manga-1-600.jpg")
binarizacaoAdaptativo(img, 'gerado/6/1-Adaptativo.png')
binarizacaoOtsu(img, 'gerado/6/1-Otsu.png')

img = cv2.imread("manga-2-300x208.jpg")
binarizacaoAdaptativo(img, 'gerado/6/2-Adaptativo.png')
binarizacaoOtsu(img, 'gerado/6/2-Otsu.png')

img = cv2.imread("manga-3-400x280.jpg")
binarizacaoAdaptativo(img, 'gerado/6/3-Adaptativo.png')
binarizacaoOtsu(img, 'gerado/6/3-Otsu.png')

print("\n Sucesso! \n Salvo dentro de gerado/6")









