from matplotlib import pyplot as plt 
import cv2
import numpy as np

def histogramaColorido(imagem, nome):
    canais = cv2.split(imagem)
    cores = ("b", "g", "r")
    plt.figure()
    plt.title("'Histograma Colorido")
    plt.xlabel("Intensidade")
    plt.ylabel("Número de Pixels")
    for (canal, cor) in zip(canais, cores):
    #Este loop executa 3 vezes, uma para cada canal
        hist = cv2.calcHist([canal], [0], None, [256], [0, 256])
        plt.plot(hist, color = cor)
        plt.xlim([0, 256])
    plt.savefig(nome)


imagem = cv2.imread("manga-1-600.jpg")
histogramaColorido(imagem, 'gerado/2/1.png')

imagem = cv2.imread("manga-2-300x208.jpg")
histogramaColorido(imagem, 'gerado/2/2.png')

imagem = cv2.imread("manga-3-400x280.jpg")
histogramaColorido(imagem, 'gerado/2/3.png')

print("\n Sucesso! \n Salvo dentro de gerado/2")