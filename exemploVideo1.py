#!/usr/bin/env python3  
# -*- coding: utf-8 -*-

import cv2

imagemCarregada = cv2.imread("exemplo.jpeg", 0)

cv2.imshow("ImagemCarregada", imagemCarregada)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite("imagemSalva.jpg", imagemCarregada)
