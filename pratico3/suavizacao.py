import numpy as np
import cv2

def suavizacaoMedia(img, nome):

    suave = cv2.blur(img, ( 7, 7))

    ret,suave = cv2.threshold(suave, 0, 255, cv2.THRESH_OTSU)

    cv2.imwrite(nome, suave)

def suavizacaoGaussiana(imagem, nome):

    suave = cv2.GaussianBlur(img, ( 7, 7), 0)

    ret,suave = cv2.threshold(suave, 0, 255, cv2.THRESH_OTSU)
    cv2.imwrite(nome, suave)

def suavizacaoMediana(imagem, nome):

    suave = cv2.medianBlur(img, 7)

    ret,suave = cv2.threshold(suave, 0, 255, cv2.THRESH_OTSU)

    cv2.imwrite(nome, suave)

def suavizacaoBilateral(imagem, nome):

    suave = cv2.bilateralFilter(img, 7, 49, 49)

    ret,suave = cv2.threshold(suave, 0, 255, cv2.THRESH_OTSU)

    cv2.imwrite(nome, suave)


img = cv2.imread("dados1.jpg")

img = img[::2,::2] # Diminui a imagem
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
suavizacaoMedia(img, 'gerado/4/1Media.png')
suavizacaoGaussiana(img, 'gerado/4/1Gaussiana.png')
suavizacaoMediana(img, 'gerado/4/1Mediana.png')
suavizacaoBilateral(img, 'gerado/4/1Bilateral.png')

img = cv2.imread("dados2.jpg")
img = img[::2,::2] # Diminui a imagem
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

suavizacaoMedia(img, 'gerado/4/2Media.png')
suavizacaoGaussiana(img, 'gerado/4/2Gaussiana.png')
suavizacaoMediana(img, 'gerado/4/2Mediana.png')
suavizacaoBilateral(img, 'gerado/4/2Bilateral.png')

print("\n Sucesso! \n Salvo dentro de gerado/4")










