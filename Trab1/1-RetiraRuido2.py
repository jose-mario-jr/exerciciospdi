import cv2
import numpy as np

# def aplica_filtro(image):
#   # grab the image dimensions
#   h = image.shape[0]
#   w = image.shape[1]
#   # loop over the image, pixel by pixel
#   for y in range(1, h-1):
#     for x in range(1, w-1):
#       # threshold the pixel
#       value = image.item(y, x, 0) + image.item(y, x, 1) + image.item(y, x, 2)
#       if value < 100 or value > 700 : 
#         neighbor = (image.item(y+1, x+1, 0), image.item(y+1, x+1, 1), image.item(y+1, x+1, 2))

#         if (neighbor[0] + neighbor[1] + neighbor[2]) > 400:  
#           image.itemset((y, x, 2), 255)
#           image.itemset((y, x, 1), 255)
#           image.itemset((y, x, 0), 255)
#         else: 
#           image.itemset((y, x, 2), neighbor[2])
#           image.itemset((y, x, 1), neighbor[1])
#           image.itemset((y, x, 0), neighbor[0])

#           # print(image.item(y+1, x+1, 0), image.item(y+1, x+1, 1), image.item(y+1, x+1, 2))

#       # image[y, x] = 255 if image[y, x] >= T else 0
#   # return the thresholded image
#   return image

def filtro_compara(im1, im2, im3):
  # grab the image dimensions
  image = im1
  h = image.shape[0]
  w = image.shape[1]
  # loop over the image, pixel by pixel
  for y in range(1, h-1):
    for x in range(1, w-1):
      # threshold the pixel
      value1 = im1.item(y, x, 0) + im1.item(y, x, 1) + im1.item(y, x, 2)
      value2 = im2.item(y, x, 0) + im2.item(y, x, 1) + im2.item(y, x, 2)
      value3 = im3.item(y, x, 0) + im3.item(y, x, 1) + im3.item(y, x, 2)

      neighbor = (image.item(y-1, x-1, 0), image.item(y-1, x-1, 1), image.item(y-1, x-1, 2))

      if value1+100 < value2 or value1+100 < value3 or value2+100 < value3 or value1-100 > value2 or value1-100 > value3 or value2-100 > value3: 
        if (neighbor[0] + neighbor[1] + neighbor[2]) > 700:  
          image.itemset((y, x, 2), 255)
          image.itemset((y, x, 1), 255)
          image.itemset((y, x, 0), 255)
        else: 
          image.itemset((y, x, 2), neighbor[2])
          image.itemset((y, x, 1), neighbor[1])
          image.itemset((y, x, 0), neighbor[0])

      
  # return the thresholded image
  return image

imagemMoedas1 = cv2.imread("moedasRuido1.jpg", cv2.IMREAD_COLOR)
imagemMoedas2 = cv2.imread("moedasRuido2.jpg")
imagemMoedas3 = cv2.imread("moedasRuido3.jpg")


fil1x = filtro_compara(imagemMoedas1, imagemMoedas2, imagemMoedas3)
fil2x = filtro_compara(imagemMoedas2, imagemMoedas3, imagemMoedas1)
fil3x = filtro_compara(imagemMoedas3, imagemMoedas2, imagemMoedas1)
fil4x = filtro_compara(imagemMoedas1, imagemMoedas2, imagemMoedas3)

cv2.imshow("fil1x", fil1x)
cv2.imshow("fil2x", fil2x)
cv2.imshow("fil3x", fil3x)
cv2.imshow("fil4x", fil4x)

cv2.waitKey(0)
cv2.destroyAllWindows()



